import threading
import time
import urllib.request, json 
import time
import threading
import numpy as np
import os

class ThreadingExample(object):
    """ Threading example class
    The run() method will be started and it will run in the background
    until the application exits.
    """

    def __init__(self, test_number, interval=1):
        """ Constructor
        :type interval: int
        :param interval: Check interval, in seconds
        """
        self.interval = interval
        self.test_number = test_number
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()                                  # Start the execution

    def run(self):
        """ Method that runs forever """
        while True:
            # Do something
            print('Doing something imporant in the background')
            dataOut = np.zeros((4,1))
            # Read data from DataNet
            with urllib.request.urlopen("http://131.155.35.53:19999/api/v1/allmetrics?format=json") as url:
                data = json.loads(url.read().decode())
            dataOut[0] = data["system.cpu"]["dimensions"]["idle"]["value"]
            dataOut[1] = data["mem.available"]["dimensions"]["MemAvailable"]["value"]
            dataOut[2] = data["system.load"]["dimensions"]["load15"]["value"]
            dataOut[3] = data["system.load"]["dimensions"]["load1"]["value"]
            # Add to file data
            file_name = 'files/output' + str(self.test_number) + '.dat'
            if os.path.exists(file_name):
                append_write = 'a' # append if already exists
            else:
                append_write = 'w' # make a new file if not
            with open(file_name, append_write) as f:
                DataToFile = np.column_stack(dataOut)
                np.savetxt(f, DataToFile, fmt=('%5.3f'))
            # Plot to matplotlib
            time.sleep(self.interval)

if __name__ == "__main__":
    example1 = ThreadingExample(test_number =2)
    while True:
        pass